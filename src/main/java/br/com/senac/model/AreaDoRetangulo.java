/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.model;


public class AreaDoRetangulo extends AreaModel{
    
    private final double A ;
    private final double B ;

    public AreaDoRetangulo(double A, double B) {
        this.A = A;
        this.B = B;
    }

    @Override
    public double getArea() {
        return this.A * this.B ;
    }


}
