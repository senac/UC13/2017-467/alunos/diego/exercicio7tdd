/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.model;


public class AreaDoTriangulo extends AreaModel{

    private double baseT;
    private double alturaT;

    public AreaDoTriangulo() {
    }

    public AreaDoTriangulo(double baseT, double alturaT) {
        this.baseT = baseT;
        this.alturaT = alturaT;
    }
    
    @Override
    public double getArea() {
        return (this.alturaT * this.baseT) / 2;
    }

    

}
