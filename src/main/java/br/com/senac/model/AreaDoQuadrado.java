/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.model;


public class AreaDoQuadrado extends AreaModel{
    
     private final double ladoQuad;
    

    public AreaDoQuadrado(double ladoQuad) {
        this.ladoQuad = ladoQuad;
    }

    @Override
    public double getArea() {
       return this.ladoQuad * this.ladoQuad;
    }
    
}
