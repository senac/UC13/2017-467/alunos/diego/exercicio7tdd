/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.model;

public class AreaDoCirculo extends AreaModel{
    
    private final double raioCirc;

    public AreaDoCirculo(double raio) {
        this.raioCirc = raio;
    }

    @Override
    public double getArea() {
        return 3.14 * (this.raioCirc * this.raioCirc) ;
    }
    
}
