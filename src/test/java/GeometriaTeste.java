/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.senac.model.AreaDoCirculo;
import br.com.senac.model.AreaDoQuadrado;
import br.com.senac.model.AreaDoRetangulo;
import org.junit.Test;
import static org.junit.Assert.*;

public class GeometriaTeste {
    
    public GeometriaTeste() {
    }
    
    
     @Test
    public void calcularAreaRetangulo (){
        
        AreaDoRetangulo areaRetangulo = new AreaDoRetangulo(30, 50);
        
        assertEquals(1500, areaRetangulo.getArea(),0.01);
        
    }
    
    @Test
    public void calcularAreaCirculo (){
        
        AreaDoCirculo areaCirculo = new AreaDoCirculo(10);
        
        assertEquals(314, areaCirculo.getArea(),0.01);
        
    }
    
     @Test
    public void calcularAreaTriangulo (){
        
         AreaDoRetangulo areaTriangulo = new AreaDoRetangulo(30, 40);
        
        assertEquals(1200, areaTriangulo.getArea(),0.01);
        
    }
    
    
    @Test
    public void calcularAreaQuadrado (){
        
        AreaDoQuadrado areaQuadrado = new AreaDoQuadrado(20);
        
        assertEquals(400, areaQuadrado.getArea(),0.01);
        
    }

    
}
